import { Form, Input, Row, Label, FormGroup, Button, Col } from "reactstrap";
import {useState} from "react"
function Register () {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [emailSignUp, setEmailSingUp] = useState("");
    const [passwordSignUp, setPasswordSignUp] = useState("");

    const changeFirstNameHandler = (event) => {
        setFirstName(event.target.value);
    };

    const changeLastNameHandler = (event) => {
        setLastName(event.target.value);
    };

    const changeSignUpEmailHandler = (event) => {
        setEmailSingUp(event.target.value);
    };

    const changeSignUpPasswordHandler = (event) => {
        setPasswordSignUp(event.target.value);
    };
    const buttonSignUp = () => {
        if(firstName === "") {
            alert("First Name is not valid");
            return false;
        }
        if(lastName === "") {
            alert("Last Name is not valid");
            return false;
        }
        if(emailSignUp === "") {
            alert("Email is not valid");
            return false;
        }
        if(passwordSignUp === "") {
            alert("Password is not valid");
            return false;
        }
        console.log("First Name: ", firstName);
        console.log("Last Name: ", lastName);
        console.log("Email: ", emailSignUp);
        console.log("Password: ", passwordSignUp);
        return true;
    };

    return (
        <>
        <Form>
            <Row className="text-center mt-5 mb-3">
                <h2 style={{color:"white"}}>
                    Sign Up For Free
                </h2>
            </Row>
            <Row>
                <Col sm={6}>
                    <FormGroup floating>
                        <Input
                        
                            id="firstname"
                            placeholder="name"
                            type="name"
                            style={{backgroundColor:"#0B5345", color:"white"}}
                            onChange={changeFirstNameHandler}
                            value={firstName}
                        />
                        <Label for="firstname" style={{color:"white"}}>
                            First Name <span style={{color:"#27AE60"}}>*</span>
                        </Label>
                    </FormGroup>
                </Col>
                <Col sm={6}>
                    <FormGroup floating>
                        <Input
                            id="lastname"
                            placeholder="name"
                            type="name"
                            style={{backgroundColor:"#0B5345", color:"white"}}
                            onChange={changeLastNameHandler}
                            value={lastName}
                        />
                        <Label for="lastname" style={{color:"white"}}>
                            Last Name <span style={{color:"#27AE60"}}>*</span>
                        </Label>
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                <FormGroup floating>
                    <Input
                        id="Email"
                        placeholder="Email"
                        type="email"
                        style={{backgroundColor:"#0B5345"}}
                        onChange={changeSignUpEmailHandler}
                        value={emailSignUp}
                    />
                    <Label for="Email" style={{marginLeft:"10px",color:"white"}}>
                        Email Address <span style={{color:"#27AE60"}}>*</span>
                    </Label>
                </FormGroup>
            </Row>
            <Row>
                <FormGroup floating>
                    <Input 
                        id="Password"
                        placeholder="password"
                        type="password"
                        style={{backgroundColor:"#0B5345", color:"white"}}
                        onChange={changeSignUpPasswordHandler}
                        value={passwordSignUp}
                    />
                    <Label for="Password" style={{marginLeft:"10px", color:"white"}}>
                        Set Password <span style={{color:"#27AE60"}}>*</span>
                    </Label>
                </FormGroup>
            </Row>
            <Row className="mt-3">
                <div >
                    <Button color="success" style={{width:"100%"}}><h3 className="mb-0" onClick={buttonSignUp}>GET STARTED</h3></Button>
                </div>
            </Row>
        </Form>
        </>
    )
}
export default Register;