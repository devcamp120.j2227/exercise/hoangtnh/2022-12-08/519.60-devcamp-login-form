import "bootstrap/dist/css/bootstrap.min.css"
import LoginForm from "./components/loginFormBody";
import {Container} from "reactstrap"

function App() {
  return (
    <Container style={{ width:"600px",marginTop:"50px", backgroundColor:"#0B5345",padding:"20px"}}>
      <LoginForm/>
    </Container>
  );
}

export default App;
