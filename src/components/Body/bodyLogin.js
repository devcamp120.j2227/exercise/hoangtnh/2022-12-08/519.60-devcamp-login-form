import { useState } from "react";
import { Form, Input, Row, Label, FormGroup, Button } from "reactstrap";

function LoginBody () {
    const [email, setEmailChange] = useState("");
    const [password, setPasswordChange] = useState("");
    const onChangeEmail = (event) => {
        setEmailChange(event.target.value);
    }
    const onChangePassword = (event) => {
        setPasswordChange(event.target.value)
    }
    const onBtnLogin = () =>{
        if((email !== "") && (password !== "") ){
            console.log("Email: " + email, "Password: " + password);
            return true;
        }
        else{
            alert("Check email and password again");
            return false;
        }
    }
    
    return (
        <>
        <Form>
            <Row className="text-center mt-5 mb-3">
                <h2 style={{color:"white"}}>
                    Welcome Back!
                </h2>
            </Row>
            <Row>
                <FormGroup floating>
                    <Input
                        id="Email"
                        placeholder="Email"
                        type="email"
                        style={{backgroundColor:"#0B5345"}}
                        onChange={onChangeEmail}
                        value={email}
                    />
                    <Label for="Email" style={{marginLeft:"10px",color:"white"}}>
                        Email Address <span style={{color:"#27AE60"}}>*</span>
                    </Label>
                </FormGroup>
            </Row>
            <Row>
                <FormGroup floating>
                    <Input
                        id="Password"
                        placeholder="password"
                        type="password"
                        style={{backgroundColor:"#0B5345"}}
                        onChange={onChangePassword}
                        autoComplete="current-password"
                        value={password}
                    />
                    <Label for="Password" style={{marginLeft:"10px", color:"white"}}>
                        Password <span style={{color:"#27AE60"}}>*</span>
                    </Label>
                </FormGroup>
            </Row>
            <Row >
                <div className="d-flex justify-content-end">
                    <a style={{color:"#27AE60", textDecoration:"none"}} href="#"><i>Forgot Password?</i></a>
                </div>
            </Row>
            <Row className="mt-3">
                <div >
                    <Button color="success" style={{width:"100%"}}><h3 className="mb-0" onClick={onBtnLogin}>LOG IN</h3></Button>
                </div>
            </Row>
        </Form>
        </>
    )
}
export default LoginBody;