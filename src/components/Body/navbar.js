import { useState } from "react";
import {Row, Button, ButtonGroup} from "reactstrap";
import LoginBody from "./bodyLogin";
import Register from "./registerBody";
function NavbarLogin (){
    const [ActiveStatusLogin, setActiveStatusLog] = useState(true);
    const [ActiveStatusRegis, setActiveStatusReg] = useState(false);
    const [ColorLog, setColorLog] = useState("success")
    const [ColorReg, setColorReg] = useState("secondary")
    const LoginButton = () =>{
            setActiveStatusLog (true);
            setActiveStatusReg (false);
            setColorLog("success")
            setColorReg("secondary")
        
    }
    const RegisterButton = () =>{
            setActiveStatusReg (true)
            setActiveStatusLog (false);
            setColorReg("success")
            setColorLog("secondary")
        
    }
    return (
        <>
        <Row>
            <ButtonGroup>
                <Button id="btn-login" color={ColorLog} onClick={LoginButton} active={ActiveStatusLogin}>
                    Log In
                </Button>
                <Button id="btn-register" color={ColorReg} onClick={RegisterButton} active={ActiveStatusRegis}>
                    Sign Up
                </Button>
            </ButtonGroup>
        </Row>
        {ActiveStatusLogin ? <LoginBody/>: <Register/>}
        </>
    )
}
export default NavbarLogin;